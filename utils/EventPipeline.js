const { EventEmitter } = require("events");

const pipeline = new EventEmitter({ captureRejections: true });

// Generic console logger for errors
pipeline.on("error", ex => {
    if(ex instanceof Error) {
        return console.error(ex.message || ex);
    }

    console.error(ex);
});

exports.handleError = ex => { pipeline.emit("error", ex) };
exports.subscribeError = handler => { pipeline.on("error", handler) };
exports.unsubscribeError = handler => { pipeline.off("error", handler) };

exports.subscribe = (event, handler) => { pipeline.on(event, handler) };
exports.unsubscribe = (event, handler) => { pipeline.off(event, handler) };
exports.publish = (event, ...data) => { pipeline.emit(event, ...data) };
