module.exports = class UserSafeError extends Error {
    constructor(type, error, parentException, log = true) {
        super(error ? error : type);
        this.log = log;

        if(error) {
            this.type = type;
        } else {
            this.type = "generic";
        }

        if(parentException) {
            this.parent = parentException;
        }
    }
}
