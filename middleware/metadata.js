module.exports = ({
    description,
    secure = false,
    deprecated = false,
    responses = {}
} = {}) => {
    let handler = (req, res, next) => next();
    handler.metadata = {
        ...handler.metadata, description, responses, secure, deprecated
    };

    return handler;
};
