const getSubdomain = (origin = "") => {
    if(!origin) return "";

    let parts = origin.split(".").reverse();

    if(origin === "127.0.0.1")
        return "";

    if(parts[0] === "localhost")
        return parts.slice(1).reverse().join(".");

    if (parts.length >= 3) //test.example.com
        return parts.slice(2).reverse().join(".");

    return "";
};

exports = module.exports = (req, res, next) => {
    try {
        let origin = req.get("origin");
        if(!origin) return next();

        req.subdomain = getSubdomain(new URL(origin).hostname);
        return next();
    } catch(ex) {
        return next(ex);
    }
};

exports.getSubdomain = getSubdomain;
