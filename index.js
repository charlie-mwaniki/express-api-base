module.exports = (app, config) => {
    if(process.env.NODE_ENV === "build") {
        let [ outputType = "", dest ] = process.argv.slice(2);

        if(config.outputType) {
            outputType = config.outputType;
        }

        switch(outputType.toLowerCase()) {
            case "openapi": require("./lib/OpenAPIBuilder")({ app, dest }); break;
            default: require("./lib/PostmanCollectionBuilder")({ app, dest }); break;
        }
    } else {
        const http = require("http");

        let server, mountpoint;
        if(config.basePath !== "/") {
            /**
             * Mounting an express app allows us to:
             * 1.) use a base path
             * 2.) not mess with Swagger/Postman docs when using a base path
             **/
            mountpoint = require("express")();

            mountpoint.use(config.basePath, app);
            server = new http.Server(mountpoint);
        } else {
            server = new http.Server(app);
        }

        return new Promise(resolve => {
            server.listen(config.api.port, () => {
                console.log(`Server now open on port ${config.api.port}.`);
                return resolve({ app, server, mountpoint });
            });
        });
    }

    return Promise.resolve();
};
